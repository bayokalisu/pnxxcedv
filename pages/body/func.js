export async function RunScript(s1, s2, b, title){
    let Nodes = document.createRange().createContextualFragment(b);
    document.getElementById('AppRoot').appendChild(Nodes.firstChild);
    const script = document.createElement('script');
    script.src = s1;
    script.async = true;
    document.head.append(script);
    script.onload = function bolder(e) {
        const script2 = document.createElement('script');
        script2.src = s2;
        document.head.append(script2);
        script2.onload = function reset_it(e) {
            document.getElementById("AppRoot").style.opacity = '1';
            document.title = title;
        }
        script2.onerror = function (event, source, lineno, colno, error) {
            console.log({event, source, lineno, colno, error})
        }
    }
    script.onerror = function (event, source, lineno, colno, error) {
        console.log({event, source, lineno, colno, error})
    }
}
